/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package capadao;

import controlador.HorariosJpaController;
import horario.entity.Horarios;

/**
 *
 * @author StevenJmnz
 */
public class tbHorarioDAO {
    
    private Horarios hora = new Horarios();
    private HorariosJpaController control = new HorariosJpaController();
    private String mensaje = "";
    public String insertarHorario(String codigo,String descripcion,String tLlegada, String tSalida ){
       try{
        hora.setId(Integer.BYTES);
        hora.setCodigo(codigo);
        hora.setDescripcion(descripcion);
        hora.setNumeroBus(0);
        hora.setTLlegada(tLlegada);
        hora.setTSalida(tSalida);
        hora.setEstado(Boolean.TRUE);
        control.create(hora);
        mensaje = "exito cargando los datos";
       } catch(Exception e){
           System.out.println("error numero" + e.getMessage());
           mensaje = "No se pudo insertar los datos";
       }
       return mensaje;  
        
    }
    public String actualizarHorario(){
        return null;
    }
    
    public String eliminarHorario(){
        return null;
    }
    
}
